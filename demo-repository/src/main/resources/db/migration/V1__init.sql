CREATE SCHEMA IF NOT EXISTS "public";
SET SCHEMA public;

CREATE TABLE tb_client (
   id INT  NOT NULL PRIMARY KEY,
   name VARCHAR(150) NOT NULL,
   credit_limit VARCHAR(150) NOT NULL,
   interest_rate TINYINT,
   risk VARCHAR(10) NOT NULL
);

-- INSERT INTO tb_client (id, name, credit_limit, interest_rate, risk) VALUES (1,'Mr. Tom',);