package com.srmasset.demo.core;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import com.srmasset.demo.repository.ClientRepository;
import org.springframework.stereotype.Component;
import  com.srmasset.demo.core.converters.ClientConverter;

import com.srmasset.demo.api.Client;
import com.srmasset.demo.repository.entity.ClientEntity;

import static java.util.stream.Collectors.toList;

@Component
public class ClientService {

    private final ClientConverter clientConverter;
    private final ClientRepository repository;

    @Inject
    public ClientService(ClientConverter clientConverter,
                         ClientRepository repository) {
        this.clientConverter = clientConverter;
        this.repository = repository;

    }

    public List<Client> getAll() {
        return repository.findAll().stream().map(entity -> clientConverter.convertToDto(entity)).collect(toList());
    }

    @Transactional
    public Client create(Client client) {
        ClientEntity entityToSave = clientConverter.convertToEntity(client);
        ClientEntity newClientEntity = repository.save(entityToSave);
        return clientConverter.convertToDto(newClientEntity);
    }


}