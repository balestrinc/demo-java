package com.srmasset.demo.core;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = DemoCoreConfiguration.class)
public class DemoCoreConfiguration {

}
