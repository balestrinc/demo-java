module.exports = function(grunt) {

    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-bower-task');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-include-source');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-cache-bust');
    grunt.loadNpmTasks('grunt-properties-reader');
    grunt.loadNpmTasks('grunt-replace');

    grunt.initConfig({
        bower: {
            install: {
                options: {
                    targetDir: './bower_components',
                    verbose: true
                }
            }
        },
        compass: {
            compile: {
                options: {
                    config: './config.rb'
                }
            }
        },
        copy: {
            vendorFonts: {
                files: [{
                        src: './src/main/webapp/assets/bower_components/font-awesome/fonts/*',
                        dest: './src/main/webapp/assets/fonts/',
                        expand: true,
                        flatten: true,
                        filter: 'isFile'
                    }, {
                        src: './src/main/webapp/assets/bower_components/bootstrap-sass-official/assets/fonts/**/*',
                        dest: './src/main/webapp/assets/fonts/bootstrap',
                        expand: true,
                        flatten: true,
                        filter: 'isFile'
                    }

                ]
            }
        },
        cssmin: {
            vendor: {
                files: {
                    './src/main/webapp/assets/css/vendor.min.css': [
                        './src/main/webapp/assets/bower_components/font-awesome/css/font-awesome.min.css',
                        './src/main/webapp/assets/bower_components/angular-ui-select/dist/select.min.css',
                        './src/main/webapp/assets/bower_components/angular-dialog-service/dist/dialogs.min.css',
                        './src/main/webapp/assets/bower_components/fullcalendar/dist/fullcalendar.min.css'
                    ]
                }
            }
        },
        uglify: {
            dist: {
                files: {
                    './src/main/webapp/assets/scripts/dist/app.min.js': './src/main/webapp/assets/js/**/*.js'
                }
            },
            vendor: {
                options: {
                    mangle: false,
                    compress: false
                },
                files: {
                    './src/main/webapp/assets/scripts/vendor/vendor.min.js': [
                        './src/main/webapp/assets/bower_components/jquery/jquery.min.js',
                        './src/main/webapp/assets/bower_components/angular/angular.min.js',
                        './src/main/webapp/assets/bower_components/angular-resource/angular-resource.min.js',
                        './src/main/webapp/assets/bower_components/angular-sanitize/angular-sanitize.min.js',
                        './src/main/webapp/assets/bower_components/angular-ui-router/release/angular-ui-router.min.js',
                        './src/main/webapp/assets/bower_components/angular-i18n/angular-locale_pt-br.js',
                        './src/main/webapp/assets/bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.js',
                        './src/main/webapp/assets/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
                        './src/main/webapp/assets/bower_components/angular-cookies/angular-cookies.min.js',
                        './src/main/webapp/assets/bower_components/angular-input-masks/releases/masks.min.js',
                        './src/main/webapp/assets/bower_components/underscore/underscore-min.js',
                        './src/main/webapp/assets/bower_components/angular-ui-select/dist/select.min.js',
                        './src/main/webapp/assets/bower_components/angular-ui-utils/ui-utils.min.js',
                        './src/main/webapp/assets/bower_components/angular-breadcrumb/dist/angular-breadcrumb.min.js',
                        './src/main/webapp/assets/bower_components/ng-file-upload/ng-file-upload-shim.min.js',
                        './src/main/webapp/assets/bower_components/ng-file-upload/ng-file-upload.min.js',
                        './src/main/webapp/assets/bower_components/checklist-model/checklist-model.js',
                        './src/main/webapp/assets/bower_components/highcharts-release/highcharts.js',
                        './src/main/webapp/assets/bower_components/angular-dialog-service/dist/dialogs.min.js',
                        './src/main/webapp/assets/bower_components/moment/moment.js',
                        './src/main/webapp/assets/bower_components/fullcalendar/dist/fullcalendar.min.js',
                        './src/main/webapp/assets/bower_components/fullcalendar/dist/lang/pt-br.js',
                        './src/main/webapp/assets/bower_components/angular-ui-calendar/src/calendar.js',
                        './node_modules/optional-js/dist/optional.min.js'
                    ]
                }
            },
        },
        includeSource: {
            dev: {
                options: {
                    basePath: './src/main/webapp/assets/js/',
                    baseUrl: '/assets/js/'
                },
                files: {
                    './src/main/webapp/view/index.html': './src/main/webapp/templates/index.tpl.html'
                }
            },
            dist: {
                options: {
                    basePath: './src/main/webapp/assets/scripts/dist/',
                    baseUrl: '/assets/scripts/dist/'
                },
                files: {
                    './src/main/webapp/view/index.html': './src/main/webapp/templates/index.tpl.html'
                }
            }
        },
        cacheBust: {
            options: {
                encoding: 'utf8',
                algorithm: 'md5',
                length: 16,
                baseDir: './src/main/webapp/',
                deleteOriginals: true
            },
            scripts: {
                files: [{
                    src: ['./src/main/webapp/view/index.html']
                }]
            }
        },
        clean: [
            "./src/main/webapp/view/index.html",
            "./src/main/webapp/assets/scripts/",
            "./src/main/webapp/assets/fonts/",
            "./src/main/webapp/assets/css/"
        ],
        karma: {
            unit: {
                configFile: 'karma.conf.js'
            }
        },
        properties: {
            app: 'src/main/resources/application.properties'
        },
        replace: {
            dist: {
                options: {
                    patterns: [{
                        match: 'systemVersion',
                        replacement: '<%= app[\"application.version\"] %>'
                    }]
                },
                files: [{
                    expand: true,
                    flatten: true,
                    src: ['./src/main/webapp/view/index.html'],
                    dest: './src/main/webapp/view/'
                }]
            }
        }
    });

    grunt.registerTask('updateVendor', ['bower', 'uglify:vendor', 'copy:vendorFonts', 'cssmin:vendor']);
    grunt.registerTask('default', ['updateVendor', 'includeSource:dev', 'properties', 'replace']);
    grunt.registerTask('dist', ['updateVendor', 'uglify:dist', 'includeSource:dist', 'cacheBust',
        'properties', 'replace', 'karma'
    ]);
    grunt.registerTask('test', ['clean', 'updateVendor', 'uglify:dist', 'karma']);
};
