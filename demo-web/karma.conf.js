module.exports = function(config) {
    config.set({
        basePath: '',
        frameworks: ['jasmine'],
        files: [
            'src/main/webapp/assets/scripts/vendor/*.js',
            'src/main/webapp/assets/scripts/dist/*.js',
            'src/main/webapp/assets/bower_components/angular-mocks/angular-mocks.js',
            'src/test/javascript/**/*.js',
            'src/main/webapp/assets/partials/**/*.html'
        ],
        exclude: [],
        reporters: ['progress'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: ['PhantomJS'],
        captureTimeout: 60000,
        singleRun: true,
        plugins: [
            'karma-jasmine',
            'karma-phantomjs-launcher',
            'karma-ng-html2js-preprocessor'
        ],
        preprocessors: {
            'src/main/webapp/assets/partials/**/*.html': ['ng-html2js']
        },
        ngHtml2JsPreprocessor: {
            moduleName: 'templates',
            stripPrefix: "src/main/webapp"
        }
    });
};