"use strict";
describe('DpeBrPeriodo', function() {

    var scope,
        $httpBackend;

    function compileElement() {

        var template = '<form name="formTeste"><div class="form-group" name="periodo" id="periodo" dpe-br-periodo ng-model="periodo"></div></form>',
            element;

        inject(function($compile) {
            element = $compile(template)(scope);
        });

        scope.$digest();

        return element;
    }

    beforeEach(module('portalDefensorApp'));

     beforeEach(module('templates'));

    beforeEach(function() {
        inject(function($rootScope, $injector) {
            scope = $rootScope.$new();
            $httpBackend = $injector.get('$httpBackend');
            $httpBackend.when('GET', '/user').respond(200);
            $httpBackend.when('GET', '/config/services-url').respond(200);
            $httpBackend.when('GET', '/assets/partials/area-de-trabalho/area-de-trabalho.html').respond(200);
        });
    });

    it('deve estar inválido quando data inicial for maior que data final', function () {
        var element;

        scope.periodo = { dataInicio: "10/10/2000", dataFim: '23/12/1999'};
        element = compileElement();

        scope.$digest();

        expect(scope.formTeste.periodo.$valid).toBeFalsy();

    });

    it('deve estar válido quando data inicial for menor que data final', function () {
        var element;

        scope.periodo = { dataInicio: "10/10/1980", dataFim: '23/12/1999'};
        element = compileElement();

        scope.$digest();

        expect(scope.formTeste.periodo.$valid).toBeTruthy();
    });

    it('deve estar válido quando data inicial for igual a data final', function () {
        var element;

        scope.periodo = { dataInicio: "23/12/1999", dataFim: '23/12/1999'};
        element = compileElement();

        scope.$digest();

        expect(scope.formTeste.periodo.$valid).toBeTruthy();
    });

    it('deve estar válido quando as duas datas estiverem preenchidas', function () {
        var element;

        scope.periodo = { dataInicio: "01/12/2015", dataFim: '31/12/2015'};
        element = compileElement();

        scope.$digest();

        expect(scope.formTeste.periodo.$valid).toBeTruthy();
    });

    it('deve estar inválido quando apenas a data inicial for preenchida', function () {
        var element;

        scope.periodo = { dataInicio: "23/12/1999"};
        element = compileElement();

        scope.$digest();

        expect(scope.formTeste.periodo.$valid).toBeFalsy();
    });

    it('deve estar inválido quando apenas a data final for preenchida', function () {
        var element;

        scope.periodo = { dataFim: '23/12/1999'};
        element = compileElement();

        scope.$digest();

        expect(scope.formTeste.periodo.$valid).toBeFalsy();
    });

    it('deve estar válido quando data inicial e final não forem preenchida', function () {
        var element;

        scope.periodo = {};
        element = compileElement();

        scope.$digest();

        expect(scope.formTeste.periodo.$valid).toBeTruthy();
    });

});
