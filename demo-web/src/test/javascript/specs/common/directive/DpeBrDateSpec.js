"use strict";
describe('DpeBrDate', function() {

    var scope,
        $httpBackend;

    function compileElement() {

        var template = '<form name="formTeste"><input type="text" class="form-control" id="inicio" name="inicio" ng-model="modelo.inicio" ui-mask="99/99/9999" dpe-br-date/></form>',
            element;

        inject(function($compile) {
            element = $compile(template)(scope);
        });

        scope.$digest();

        return element;
    }

    beforeEach(module('portalDefensorApp'));

    beforeEach(function() {
        inject(function($rootScope, $injector) {
            scope = $rootScope.$new();
            $httpBackend = $injector.get('$httpBackend');
            $httpBackend.when('GET', '/user').respond(200);
            $httpBackend.when('GET', '/assets/partials/area-de-trabalho/area-de-trabalho.html').respond(200);
            $httpBackend.when('GET', '/config/services-url').respond(200);
        });
    });

    it('deve inicializar diretiva corretamente"', function () {
        var element;

        scope.modelo = { inicio: "10/10/2015"};
        element = compileElement();

        expect(element.find('#inicio').attr("ng-pattern")).not.toBeNull();

        expect(element.find('#inicio').siblings('.dpe-input-invalid').prop("outerHTML")).toEqual(jasmine.stringMatching(/^<span .*>Data inválida<\/span>$/));

    });

    it('deve exibir mensagem de erro quando data invalida e campo perdeu foco"', function () {
        var element;

        scope.modelo = { inicio: "10/60/2015"};
        element = compileElement();

        scope.$digest();

        element.find('#inicio').triggerHandler('blur');

        expect(element.find('#inicio').siblings('.dpe-input-invalid').hasClass("ng-hide")).toBeFalsy();

    });

    it('não deve exibir mensagem de erro quando data valida e campo perdeu foco"', function () {
        var element;

        scope.modelo = { inicio: "10/12/2015"};
        element = compileElement();

        scope.$digest();

        element.find('#inicio').triggerHandler('blur');

        expect(element.find('#inicio').siblings('.dpe-input-invalid').hasClass("ng-hide")).toBeTruthy();

    });
});
