package com.srmasset.demo.web.controllers;


import com.srmasset.demo.api.Client;
import com.srmasset.demo.core.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import java.util.List;


@RestController
public class ClientController {

    public static final Logger logger = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private final ClientService service;

    public ClientController(ClientService service) {
        this.service = service;
    }

    @RequestMapping(method=GET, value ="/client")
    public ResponseEntity<List<Client>>  clients() {
        List<Client> all = service.getAll();
        return new ResponseEntity<List<Client>>(all, HttpStatus.OK);
    }

    @RequestMapping(value = "/client", method = POST)
    public ResponseEntity<Client>  createUser(@RequestBody Client client) {
        logger.info("Creating client : {}", client);
        Client newClient = service.create(client);
        return new ResponseEntity<Client>(newClient, HttpStatus.CREATED);
    }

}
