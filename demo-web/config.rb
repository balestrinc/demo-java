require 'compass/import-once/activate'

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "src/main/webapp/assets/css"
sass_dir = "src/main/webapp/assets/scss"
images_dir = "src/main/webapp/assets/images"
javascripts_dir = "src/main/webapp/assets/js"

# output_style = :expanded or :nested or :compact or :compressed
output_style = :compressed
# To enable relative paths to assets via compass helper functions.
relative_assets = true
